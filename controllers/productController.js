const Product = require ("../models/Products");
const auth = require ("../auth.js");

//Adding new product
//By admin only
module.exports.addNewProduct = (req, res) => {

		const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	if(userData.isAdmin) {
	
		let newProduct = new Product({
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			stocks: req.body.stocks
		});


		return newProduct.save()
		.then(course => {
			console.log(course);
			res.send({message:"You have successfully added a product!"});
		})
	
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	} else {
		return res.send({message:"You are not authorized to add a product."});
	}
	
}

//Retireve all products
// Admin only
module.exports.getAll = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	

	if(userData.isAdmin) {
		return Product.find({}).then(result => res.send(result));
	} else {
		return res.send(false);
	}
}


// Retrieve all active products
//All users

module.exports.getAllActive = (req, res) => {
	return Product.find({isActive: true}).then(result => res.send(result));
}

// Retrieve all inactive products
//All users

module.exports.getAllInactive = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin) {
	return Product.find({isActive: false}).then(result => res.send(result));
} else {
		return res.send(false);
	}
}
// Retrieve a sepcific product
//All users

module.exports.getProduct = (req, res) => {

	console.log(req.params.productId)

	return Product.findById(req.params.productId)
	.then(result => {
		console.log(result);
		return res.send(result)
	})
	.catch(error => {
		console.log(error);
		return res.send(error);
	})
}

// Update product information
// By Admin only

module.exports.updateProduct = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin) {
		let updateProduct = {
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			stocks: req.body.stocks
		}

		return Product.findByIdAndUpdate(req.params.productId, updateProduct, {new:true}).then(result => {

			console.log(result);
			res.send (result);
		})
	.catch(error => {
		console.log(error);
		res.send(false);
	})
	} else {
		return res.send(false);
	}
}

/*module.exports.updatePic = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin) {
		let updatePic = {
			pictureSrc: req.body.picture
		}

		return Product.findByIdAndUpdate(req.params.productId, updatePic, {new:true}).then(result => {

			console.log(result);
			res.send (result);
		})
	.catch(error => {
		console.log(error);
		res.send(false);
	})
	} else {
		return res.send(false);
	}
}*/

// Archiving & Unarchiving Product 
// By Admin only

module.exports.archiveProduct = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin) {
		let archiveProduct = {
			isActive: req.body.isActive
		}

		return Product.findByIdAndUpdate(req.params.productId, archiveProduct, {new:true}).then(result => {
			console.log(result);
			res.send(true);
		})
		.catch(error =>{
			console.log(error);
			res.send(false);
		})
 	} else {
 		return res.send(false);
 	}
 }

 /*
Stretch Goals
1. Set user as admin (Admin only)
2. Retrieve authenticated user’s orders
3. Retrieve all orders (Admin only)
4. Add to Cart
5. Added Products
6. Change product quantities
7. Remove products from cart
8. Subtotal for each item
9. Total price for all items
*/


