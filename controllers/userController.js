const User = require("../models/User");
const Product = require("../models/Products");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");


module.exports.checkEmailExists = (req, res) => {
	return User.find({email: req.body.email}).then(result => {

		// The "find" method return a record if a match is found
		if(result.length > 0) {
			return res.send (true);

		// No duplicate email found
		// The user is not yet registered in the database
		} else {
			return res.send (false);
		}
	})
	.catch(error => res.send (error))
}
// New User Registration
module.exports.registerUser = (req, res) => {

	 let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		address: req.body.address,
		email: req.body.email,
		contactNumber: req.body.contactNumber,
		password: bcrypt.hashSync(req.body.password, 10)
	})

	 return newUser.save().then(user => {
	 	console.log(user);
	 	res.send({message:"You have successfully registered!"})
	 })
	 .catch(error => {
	 	console.log(error);
	 	res.send(false);
	 })
}

// User authentication

module.exports.loginUser = (req, res) => {
	
	return User.findOne({email: req.body.email}).then(result => {

		if (result == null) {

			return res.send({message: "No user found."});

		} else {

			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);


		if (isPasswordCorrect) {
			console.log("User successfully logged in.");
			return res.send({accessToken: auth.createAccessToken(result)});


		} else {
			console.log(false);
			return res.send({message: "Incorrect password."})
		}
		}
	})
}

// Retrieve user details
module.exports.getProfile = (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	return User.findById(userData.id).then(result => {
		result.password = "";
		return res.send(result);
	});

};

/// Make order
// Non-admin only

module.exports.makeOrder = async (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(!userData.isAdmin) {
		
		let productName = await Product.findById(req.body.productId)
		.then(result => result.name);


		let data = {

			userId: userData.id,
			address: userData.address,
	
			productId: req.body.productId,
			quantity: req.body.quantity
		}
		console.log(data);
		

		/*let orderQuantity = await User.findById(data.userId).then(
			).then(result => result.quantity);
			let quantity= req.body.quantity*/

		let isUserUpdated = await User.findById(data.userId).then(user => {


			user.orders.push({
				productId: data.productId,
				quantity: data.quantity
				
			});

			return user.save().then(result => {
				console.log(result);
				return true;
			})
			.catch(error => {
				console.log(error);
				return false;
			});
		});
		console.log(isUserUpdated);


		let isProductUpdated = await Product.findById(data.productId)
		.then(product => {

			product.orders.push({
				userId: data.userId,
				quantity: req.body.quantity
			})
		
			product.stocks -= req.body.quantity;

			return product.save().then(result => {
				console.log(result);
				return true;
			})
			.catch(error => {
				console.log(error);
				return false;
			});
		});
		console.log(isProductUpdated);

		if(isUserUpdated && isProductUpdated) {
			return res.send(true);

		} else {
			return res.send(false);
		}
	} else {
		return res.send(false);
	}

	

}


/*
Stretch Goals
1. Set user as admin (Admin only)
2. Retrieve authenticated user’s orders
3. Retrieve all orders (Admin only)
4. Add to Cart
5. Added Products
6. Change product quantities
7. Remove products from cart
8. Subtotal for each item
9. Total price for all items
*/

// Set user as admin
module.exports.makeAdmin = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin) {
		let makeAdmin = {
			isAdmin: req.body.isAdmin
		}

		return User.findByIdAndUpdate(req.params.userId, makeAdmin, {new:true}).then(result => {
			console.log(result);
			res.send(true);
		})
		.catch(error =>{
			console.log(error);
			res.send(false);
		})
 	} else {
 		return res.send(false);
 	}
 }

//Retrieve authenticated user’s orders

module.exports.viewOrder = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	return User.findById(userData.id).then(result => {
		
		return res.send(result);
	});
	
}
/*module.exports.viewCart = (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	return User.findById(userData.id).then(result => {
		result.password = "";
		return res.send(result);
	});

};*/