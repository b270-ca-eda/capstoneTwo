const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require ("../auth");

router.post("/checkEmail", userController.checkEmailExists);
//Route for user registration
router.post("/register", userController.registerUser);

//Route for user authentication
router.post("/login", userController.loginUser);

//Route for retreiving user information
router.get("/details", auth.verify, userController.getProfile);

//Route for making order
router.post("/order", auth.verify, userController.makeOrder);

// Stretch Goals

// Route to make user an admin
router.patch("/admin/:userId", auth.verify, userController.makeAdmin);

//Route for retreiving user orders
router.get("/cart", auth.verify, userController.viewOrder);

module.exports = router;