const express = require("express");
const router = express.Router();
const productController = require ("../controllers/productController");
const auth = require ("../auth");

// Route for creating a product
router.post("/new", auth.verify, productController.addNewProduct);

// Route for retrieving all active products
router.get("/active", productController.getAllActive);

// Route for retrieving all inactive products
router.get("/inactive", productController.getAllInactive);

//Rout for retrieiving all products
router.get("/all", auth.verify, productController.getAll);

//Route for retrieving a specific product
router.get("/:productId", productController.getProduct);

//Route for updating produuct information
router.put("/update/:productId", auth.verify, productController.updateProduct);

//Route for updating pic
// router.put("/upload/:productId", auth.verify, productController.updatePic);

// Route for arching and unarchiving a product
router.patch("/archive/:productId/", auth.verify, productController.archiveProduct);


module.exports = router;