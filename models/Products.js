const mongoose = require ("mongoose");

/*
	name - string
	description - string
	price - number
	isActive - boolean, default true
	createdOn - date, default to current timestamp
	orders - array
		orderId - string	
*/

const productSchema = new mongoose.Schema ({
	name: {
		type: String,
		required: [true, "Product name is required."]
	},
	description: {
		type: String,
		required: [true, "Product description is required."]
	},
	price: {
		type: Number,
		required: [true, "Price is required."]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date
	},

	stocks: {
		type: Number,
		required: [true, "Number of stocks is required!"]
	},

	/*pictureSrc: {
		type: String,
		required: [false, "Number of stocks is required!"]
	},*/

	orders:  [
		{
			userId: {
				type: String,
				required: [true, "User ID is required."]
			},
			
			quantity: {
				type: Number,
				required: false
			},
			isPaid: {
				type: Boolean,
				default: true
			},
			dateOrdered: {
				type: Date,
				default: new Date()
			}
		}
	]
})
module.exports = mongoose.model("Product", productSchema);