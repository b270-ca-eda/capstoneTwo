const mongoose = require("mongoose");


/*
email - string
password - string
isAdmin - boolean, default to false
orders - array:
	product - array
		product name - string
		quantity
	totalAmount - number
	purchasedOn - date, default to current timestamp
*/

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "Your first name is required!"]
	},
	lastName: {
		type: String,
		required: [true, "Your last name is required!"]
	},
	address: {
		type: String,
		required: [true, "Address is required!"]
	},
	email: {
		type: String,
		required: [true, "Email is required!"]
	},
	contactNumber: {
		type: String,
		required: [true, "Contact number is required!"]
	},
	password: {
		type: String,
		required: [true, "Password is required!"]
	},
	isAdmin: {
		type: Boolean,
		default: false

	},
	
	

	orders: [
		{
			productId: {
				type: String
				
			},

			quantity: {
				type: Number,
				required: false
			},

			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);