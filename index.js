// Dependencies
const express = require ("express");
const mongoose = require ("mongoose");
const cors = require ("cors");
const app = express();
const userRoutes = require("./routes/userRoutes");
const productRoutes = require ("./routes/productRoutes");
const router = express.Router();
/*const courseRoutes = require("./routes/courseRoutes");*/

//Database connection
mongoose.connect("mongodb+srv://admin:admin123@zuitt.1bvjamd.mongodb.net/b270_capstone2_eCommerceAPI?retryWrites=true&w=majority",	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}	
);

// Set notification for database connection success or failure 
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log("We're connected to the cloud database"))

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/users", userRoutes);
app.use("/products", productRoutes);

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`);
})
